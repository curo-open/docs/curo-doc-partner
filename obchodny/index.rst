Obchodný zástupca
=================

.. toctree::
   :maxdepth: 3
   :caption: Obsah:

   install


Marketingové materiály
----------------------

.. todo:: dať k dispozícii materiály ako logo, reklamné plagáty

Inštalácia
----------

.. todo:: ako skontrolovať PC a nainštalovať Curo u lekára
.. todo:: ako prejsť z DEMO databázy na reálnu + ako požiadať o aktiváciu licencie ?
.. todo:: ako otestovať eZdravie server ?

Podstatné funkcie
-----------------
.. todo:: funkcie, na ktoré upozorňovať zákazníkov

Základný workflow na začiatok
-----------------------------

.. todo:: krátka ukážka ako začať

Ďalšie kroky
------------

.. todo::
   * nastaviť zálohovanie
   * zdielanie súborov
   * konfigurácia tlačiarní
   * požiadať o aktiváciu licencie

Model podpory zákazníka
+++++++++++++++++++++++

.. todo:: vysvetlenie ako má zákazník kontaktovať podporu a kedy sa má obrátiť na Curo a kedy na zástupcu


Odpovede na bežné otázky
------------------------


