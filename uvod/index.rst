.. _uvod:

O príručke
==========

Komu je príručka určená
-----------------------

Curo sa snaží byť otvorený systém. Hľadáme možnosť prakticky sa prepájať s každým v systéme zdravotníctva, 
aby z toho mala čo najväčší úžitok ambulancia a liečba pacienta.
Ten kto má záujem s nami spolupracovať považujeme za **partnera Curo**.

V nasledovnom diagrame nájdete všetko, čo partner Curo potrebje. Pokiaľ ste **bežný užívateľ** dozviete sa v ňom, 
čo pre ambulanciu môže spraviť partner Curo, ale ďalej pre vás táto príručka nie je. Prejdite na dokumentáciu pre užívateľa.

.. uml:: urcenie.plantuml
   :caption: Každý môže byť **partnerom Curo**

Ako čítať túto príručku
------------------------

Zlepšenie tejto dokumentácie
++++++++++++++++++++++++++++