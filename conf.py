# Configuration file for the Sphinx documentation builder.
#
# Full list of options: http://www.sphinx-doc.org/en/master/config

# -- Use shared config
import os
import sys
sys.path.append(os.path.abspath('./_config'))

# from sphinxconf import *

# -- Project information -----------------------------------------------------

project = 'Curo pre partnerov'
author = 'Curo tím'
language = 'sk'
html_title = 'Curo pre partnerov'
master_doc = 'index'

# Version info for the project, acts as replacement for |version| and |release|
# The short X.Y version
# version = 'latest'
# The full version, including alpha/beta/rc tags
# release = 'latest'


# VCS options: https://docs.readthedocs.io/en/latest/vcs.html#github
html_theme = 'sphinx_rtd_theme'
html_context = {
    "display_gitlab": True, # Integrate Gitlab
    "gitlab_user": "curo-open", # Username
    "gitlab_repo": "docs/curo-doc-partner", # Repo name
    "gitlab_version": "master", # Version
    "conf_py_path": "/", # Path in the checkout to the docs root
}

latex_documents = [
  ("index", 'manual.tex', html_title, author, 'manual'),
]

epub_title = project
epub_author = author
epub_publisher = author
# epub_copyright = copyright