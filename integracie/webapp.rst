.. _webapp:

Externé web aplikácie registrované ako akcie
============================================

Konfigurácia ambulancie umožnuje pridať do vyhľadávacieho riadu `@tlač` odkazy na webové stránky (aplikácie).
Prepojením napr. na `Google Sheets <https://www.google.sk/intl/sk/sheets/about/>`_ je týmto spôsobom možné vytvoriť vlastné riešenia evidencie (napr.
sterilizačný denník, sklad, kniha jázd), prípadne odkaz na online riešenie účtovníctva a fakturácie.

Takto otvorená web stránka môže vyžadovať prihlásenie užívateľa, ktoré sa po úspešnom prihlásení bezpečne zapamätaná. 
Takéto prihlásenie musí realizovať každý užívateľ Curo samostatne, nie je možné ho automatizovať zo strany Curo.

.. warning::
   V súčasnosti Curo sprístupňuje **tlačové výstupy** ako aj **iné akcie** a **integrácie**
   pod vyhľadávacím kľúčom `@tlač`. Do budúcnosti budú akcie zobrazené vo vlastnom zozname.

Požiadavka na podpora@curo.sk o registráciu odkazu musí obsahovať:

* **názov**: text zobrazený a vyhľadateľný vo vyhľadávacom riadku Curo
* **typ**: o aký typ stránky sa jedná (tlač / akcia)

Ďalej je možné upresniť:

* **kontext**: kedy sa má zobraziť (vždy / na pacientovi / na dekurze / na type sekcie)
* **parametre**: ako a ktoré dáta z Curo odovzdať otváranej stránke 

.. todo:: dopísať ako vytvoriť link na zostavu/akciu z nástenky ambulancie

.. todo:: 
   nájsť viac využití ako https://www.zoho.com/sheet/, https://etherpad.org/, https://calendar.google.com/calendar/r
