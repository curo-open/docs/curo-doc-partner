.. _protocolCuro:

Protocol curo://
----------------

Tento protokol umožňuje jednoduchým spôsobom vyvolať v prostredí :term:`CuroUI` niektorú z nižšie uvedených akcií.
Počas inštalácie je protokol zaregistrovaný do operačného systému (:program:`Windows`/:program:`Linux`/:program:`MacOS`), 
čo umožnuje akejkoľvek stránke zobrazenej web browserom odovzdať inštrukcie do :term:`CuroUI`.
Pokiaľ je :term:`CuroUI` na PC spustené je možné mu inštrukciu odovzdať opätovným spustím :term:`CuroUI` 
s parametrom začínajúcim na `curo:` (resp. `curo://`).

Možné využitie:

* VoIP telefón otvorí kartu pacienta (napr. `curoui.exe "curo://query#@pacient tel=+421911111111"`)
* pri písaní dokumentácie a krátkych návodov je možné užívateľa linkom vykonať za užívateľa akciu v CuroUI
* pri vytváraní vlastného widgetu, ktorý sa stane súčasťou CuroUI

.. todo:: viac príkladov, napr. z dashboard návodu
.. todo:: zdokumentovať protokol curo://
