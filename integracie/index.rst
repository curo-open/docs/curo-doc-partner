Integrácie s externými systémami
========================================

.. toctree::
   :maxdepth: 2
   :caption: Obsah:

   protocolCuro
   devices
   webapp
   widget
   messaging
   exchange
   extendcs
