.. _devices:

Externé zariadenia
==================

Curo obsahuje definície bežne používaných externých programov, 
ktoré má zmysel mať prepojené s ambulančným systémom. Prepojenie môže byť jednosmerné alebo obojsmerné.

Obojsmerné prepojenie znamená, že Curo zavolá aplikáciu a tá má možnosť vrátiť dáta použiteľé pri vytváraní dekurzu.
Takáto komunikácia v súčasnosti vyžaduje programátorské práce. V prípade potreby napíšte na podpora@curo.sk.

V prípade jednosmerného prepojenia, Curo zavolá program s parametrami, čím komunikácia končí.
Takéto programy si vie užívateľ nakonfigurovať s ale aj bez podpory autorov programu.
Je potrebné vytvoriť v :term:`konfiguračnom adresári<konfiguračný adresár CuroUI>` súbor `zdroje.json` podľa nasledovného vzoru.

.. literalinclude:: zdroje.json
   :caption: 
   :language: json
   :emphasize-lines: 8-11,13-16,25-28,36

Poznámky k formátu:

* každé prepojenie má svoj názov (`NAPS2`, `SIP`)
* názov záznamu nemá začínať podčiarkovníkom, pretože to je využívané pre definície zahrnuté v distribúcii Curo.
* `title` a `description` sa zobrazuje užívateľovi vo výsledkoch `@zdroj`
* `bin` je cesta k programu, ktorý sa spustí

  * `bin` nemusí obsahovať presnú cestu ak sa program dá v operačnom systéme vyhľadať v predkonfigurovaných cestách (`env.PATH`)
  * záznam sa v `@zdroj` nezobrazí pokiaľ nebude možné na disku nájsť súbor `bin`

* `params` obsahuje zoznam parametrov, s ktorými sa program spustí
* parametere na zvýraznených riadkoch obsahujú premenné, ktoré sa doplnia aktuálnych dát nahraných v Curo

  * parametre sa dopĺňajú len v prípade, že parameter je pole, kde druhá položka je `true`
  * napr. `"{p.lastName}"` sa odovzdá bez substitúcie
  * ale v `[ "{p.lastName}", true ]` bude doplnene priezvisko
  * niektoré znaky musia byť špeciálne zadané v JSON formáte, odporúčame použiť napr. https://www.freeformatter.com/json-escape.html

Niektoré atribúty a ich vlastnosti:

+----------------------+------------------------------------------------------------------------------------+
| Atribút              | Popis                                                                              |
+======================+====================================================================================+
| `p.lastName`         | priezvisko pacienta                                                                |
+----------------------+------------------------------------------------------------------------------------+
| `p.firstName`        | meno pacienta                                                                      |
+----------------------+------------------------------------------------------------------------------------+
| `p.rc`               | rodné číslo pacienta bez limítka a medzier                                         |
+----------------------+------------------------------------------------------------------------------------+
| `p.telephone`        | telefónne číslo pacienta v medzinárodnom formáte (napr. `00421904005000`)          |
+----------------------+------------------------------------------------------------------------------------+
| `s.time`             | dátum vytvorenia dekurzu, hodnotu je možné formátovať `{s.time,YY_MM_DD_HH_mm_ss}` |
+----------------------+------------------------------------------------------------------------------------+
