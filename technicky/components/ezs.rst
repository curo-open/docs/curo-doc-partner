.. _ezs:

Curo eZdravie Server
====================

Inštalácia
----------

#. najprv potrebujeme nainštalovať eZdravie komponent z NCZI, `priamy link <https://www.ezdravotnictvo.sk/sk/-/pzs-instalacia-citacky-a-karty-epzp>`_, podľa pdf návodu na stránke treba zabezpeČiť použitie spravného ovladača pre kartu a čitačku. Komponent po rozbalení musíte inštalovať ako správca.
#. zasuňte čítačku a kartu do počitača a pockajte kym ju systém rozozná
#. ďalšim krokom je nainštalovať CURO eZdravie Server, aplikáciu viete `stiahnuť tu <https://www.curo.sk/w/ezdravie>`_
#. po úspešnom štarte aplikácie v systém tray kliknite pravým tlačidlo myši na ikonku eZdravia servera a nastavte automatický štart aplikacie pri štarte systemu Windows

Testovanie
----------

#. v systém tray kliknite pravým tlačidlo myši na ikonku eZdravia servera a vyberte položku `Informácie`
#. obrazovka by nemala obsahovať žiadne červené chybové texty
#. následne kliknite na tlačidlo diagnostika, sytém Vás vyzve na zadanie pinu
#. v zobrazenom výsledku pri úspešnom teste by sa nemali vyskytovať žiadne červené chybové texty
#. ak pridávate nového lekára, je potrebné aby ste zavolali na podporu a lekár bol pridaný do configurácie podporou

Testovanie pomocou skriptu
--------------------------

Program slúži na interakciu s eZdravie serverom z príkazového riadku, alebo zo scriptov.
Pre OS Windows, Linux, MacOS sú  `skompilované verzie programu tu <https://curoapp.s3.eu-west-1.amazonaws.com/apps/ezdravie-testapp.zip>`_.
V archív obsahuje binárne programy (je to vždy jeden súbor bez nutnosti inštalovať).

Volanie `ezs-cmd --help` zobrazí sub-príkazy a parametre.

Parametre príkazového riadku je možné nahradiť konfiguračným súborom a environment premennými. Priorita zdrojov konfigurácie je (vrchnejšie prebíja nižšie):

#. príkazový riadok
#. enviroment premenné
#. konfiguračný súbor

**Príkazový riadok**

+------------------------+--------------------------------------------------------------------------------------------------------------+
| Sub-príkaz             | Účel                                                                                                         |
+========================+==============================================================================================================+
| help                   | Zobrazí nápovedu.                                                                                            |
+------------------------+--------------------------------------------------------------------------------------------------------------+
| version                | Zobrazí verziu programu.                                                                                     |
+------------------------+--------------------------------------------------------------------------------------------------------------+
| diag                   | Pripojí sa na eZS a vykoná diagnostiku, v prípade problémov vráti exit kód !=0.                              |
+------------------------+--------------------------------------------------------------------------------------------------------------+
| restart                | Pripojí sa na eZS a požiada ho o samo reštart (programu, nie operačného systému). Exit kod !=0 znamená chybu.|
+------------------------+--------------------------------------------------------------------------------------------------------------+

**diag**

Parameter `--restart` zabezpečí, že v prípade detekcie chyby počas diagnostiky nastane reštart eZS servera.

Vykonajú sa nasledovné testy:

#. pripojenie na eZS
#. zistenie verzie eZS
#. zistenie spôsobu pripojenia a či existuje konektivita na základné end pointy bez ich volania
#. zistenie identifikátova vloženej HPRO karty
#. volanie DajOUPZS

**Environment premenné**

Musia začínať `EZSCMD_` a píše sa celé veľkými písmenami. Názov premennej je zhodný s parametrami na príkazovom riadku. Napr.:
`EZSCMD_SERVER=localhost:5555 ezs-cmd diag`


Číťačka a karta na inom počítači ako CURO MD
--------------------------------------------

#. na počitači kde je Curo MD server v súbore `curo-md.config.js` v roote `C:\ProgramData\curo`
#. v sekcii `ezdravie` doplnte IP adresy alebo sieťove meno počítača na ktorom sa nachádza čítačka a eZdravie server príklad nižšie
#. ak použijete IP adresu mali by ste ju nastaviť ako statickú
#. vo firewalle potrebujete otvorit port 50051 pre incoming connection

.. code-block:: javascript
  :linenos:
  :emphasize-lines: 3,4

  ezdravie: {
    'servers': [
      'localhost:50051',
      '192.168.1.15:50051'
    ],
    disable: true
  }


Riešenia chybových stavov
-------------------------

* ak sa pri provom spusteni objaví chyba ohľadom `nemožnosti zmeny prod prostredia` potrebujete prvý krát `spustiť aplikácia ako Správca`
* ak testy neprechádzaju je potredne stiahnuť `NCZI testovaciu aplikáciu <https://curoapp.s3-eu-west-1.amazonaws.com/releases/beta/windows/TestProject_2.15.0.zip>`_ aspustiť základny test a overiť či systémopve požiadavky sú splnené