.. _components:

Komponenty Curo
===============

.. toctree::
   :maxdepth: 2
   :caption: Obsah:

   ./curomd
   ./curoui
   ./ezs

Curo pozostáva z niekoľkých komponentov :term:`CuroMD`, :term:`CuroUI`, :term:`Curo eZdravie`, ktoré
sa inštalujú samostatne podľa potreby na jednotlivé PC v ambulancii. 

Táto kapitola sa venuje jednotlivým komponentom Curo na úroveni technických detailov (inštalácia, spustenie, konfigurácia). 

Inštalačné programy
-------------------

Aktuálne inštalačné programy pre rôzne operačné systémy sú dostupné na webovej stránke https://www.curo.sk/software/. 

Komponenty sa inštalujú samostane, jednoduchý návod ako zinštalovať Curo v ambulancii nájdete v kapitole :ref:`simpleinstall`. 
Jedine inštalácia :term:`Curo eZdravie` je závislá od inštalácie ćudzieho komponentu od NCZI (bezpečnostný kompnent a ovládače čítačiek HPRO karieť).

=========  ===================
Komponent  Kam nainštalovať
=========  ===================
CuroMD     - samostaný server, musí byť zapnutý, aby sa dalo pracovať z iných počítačov
           - obsahuje dáta, preto bezpečné disky a prostriedky na zálohovanie
CuroUI     - na každom PC, z ktoré sa má pracovať s dátami amulancie
           - vrátane PC, kde je CuroMD, pokiaľ to nie je dedikovaný server
eZdravie   - na každé PC sa dá pripojiť len jedna čítačka HPRO karie (techincké obmedzenie NCZI)
           - pre využitie je nutné na to isté PC nainštalovať tento komponentom
           - prostredníctvom tejto komponenty komunikuje CuroMD s eZdravie, pre komunikáciu si v sieti nájde inštanciu s príslušnou HPRO kartou
=========  ===================

Inštalačné balíky sú vytvorené programom NSIS, preto je možné ich **spúštať aj automaticky**, bez zobrazenia informácií užívateľovi. 
K tomu využite parametre zdokumentované v kompilátore inštalácii `NSIS <http://www.silentinstall.org/nsis>`_.

.. todo::  instalácia pomocou http://chocolatey.org/
