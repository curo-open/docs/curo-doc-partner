.. _curomd:

.. role:: bash(code)
   :language: bash

CuroMD
======

Inštalácia
----------

Návod nájdete v kapitole :ref:`simpleinstall` .

V prípade že chcete data programu Curo mať na icom disku ako C: môžte použiť cmd parameter počas instalácie

https://nsis.sourceforge.io/Which_command_line_parameters_can_be_used_to_configure_installers%3F

.. todo::  vysvetlit instalácia allusers vs. currentuser

Viac ambulancií na rovnakom PC
------------------------------

:term:`CuroMD` umožňuje prevádzkovať viaceré ambulancie v rámci jednej databázy.
Takáto konfigurácia zdieľa zoznam pacientov a nie je vhodná, pokiaľ majú byť dáta pacientov úplne oddelené.

Na jednom PC je možné spustiť **viaceré inštancie CuroMD**. Pri tom nie je potrebné inštalovať aplikáciu viackrát. Postačuje splniť nasledovné podmienky:

#. `CuroMD.exe` musí byť spustené s parametrom `--multiple`
#. každá inštancia `CuroMD.exe` musí používať odlišný adresár dát ambulancie
#. každá inštancia `CuroMD.exe` musí pre komunikáciu s CuroUI používať odlišný TCP port
#. každá inštancia `CuroMD.exe` používať odlišný TCP port pre lokálnu komunikáciu s databázou

**Príklad spustenia 2. inštancie:**

* vytvorte prázdny adresár, napr. `c:\\ProgramData\\curo-2`
* vytvorte súbor `c:\\programdata\\curo-2\\curo-md.config.js`

.. code-block:: javascript

   module.exports = {
     port: 3001,
     pouchdb: {
       port: 5985
     }
   }

* spustite server :bash:`curomd.exe --multiple --data "c:\ProgramData\curo-2"`

Dôležité parametre
------------------

+------------------------+---------------------+---------------------------------+
| Parameter              | Konfigurácia        | Bežná hodnota                   |
+========================+=====================+=================================+
| adresár inštalácie     |                     |`C:\\Program Files (x86)\\CuroMD`|
+------------------------+---------------------+---------------------------------+
| adresár dát ambulancie | - `--data`          | `C:\\ProgramData\\curo`         |
|                        | - `CURO_PATH_ROOT`  |                                 |
+------------------------+---------------------+---------------------------------+
| komunikačný TCP port   | - `SERVER_PORT`     | `3000`                          |
|                        | - `config.port`     |                                 |
+------------------------+---------------------+---------------------------------+
| komunikačná TCP adresa | - `SERVER_HOST`     | `0.0.0.0`                       |
|                        | - `CONFIG.host`     |                                 |
+------------------------+---------------------+---------------------------------+

+------------------------+-----------------------+-------------------------------------+
| interný TCP port       | - `CURO_POUCHDB_PORT` | `5984`                              |
| databázy               | - `CFG.pouchdb.port`  |                                     |
+------------------------+-----------------------+-------------------------------------+
| zobrazenie nápovedy    | `--help`              |                                     |
| pre curomd.exe         |                       |                                     |
+------------------------+-----------------------+-------------------------------------+
| čitateľnejší výstup    | `--log-pretty-print`  |                                     |
| do konzoly             |                       |                                     |
+------------------------+-----------------------+-------------------------------------+
| Start MD pomocou UI    |`CURO_DISABLE_MD_START`| `1`                                 |
+------------------------+-----------------------+-------------------------------------+
| adresár s obrázkami    |`CURO_DOCUMENTS_PATH`  | `setx CURO_DOCUMENTS_PATH           |
|                        |                       | <UNC cesta k CURO_PATH_ROOT\storage`|
+------------------------+-----------------------+-------------------------------------+

.. todo::  zjednotit nazvy cmd a env (CURO\_) parametrov a tiez noveho config uloziska

CuroMD ako Windows Service
--------------------------

Inštalácia :term:`CuroMD` obsahuje všetky potrebné nástroje na spustenie ako :term:`Windows Service`.
Postačuje manuálne zaregistrovať CuroMD ako servis spustením príkazu:

:bash:`"C:\\Program Files (x86)\\CuroMD\\resources\\CuroMdService.exe" install`

Služba je vytvorená prostredníctvom open source projektu `WinSW <https://github.com/kohsuke/winsw>`_.
Je definovaná v súbore `C:\\Program Files (x86)\\CuroMD\\CuroMdService.xml"`, ktorého formát
je `zdokumentovaný <https://github.com/kohsuke/winsw/blob/master/doc/xmlConfigFile.md>`_ na stránkach projektu WinSW.

Súbor v tomto **adresári nemente**, bude prepísaný pri aktualizáciu, konfiguráciu servisu môžete skopírovať do iného adresára a tam ho upraviť.


Viac inštancií ako Windows Service
+++++++++++++++++++++++++++++++++++

Tak ako uvedené na začiatku tejto kapitoly, je nutné aj pri spustení ako služba
nastaviť niektoré parametre odlišne pre každú inštanciu CuroMD.

V príklade vyššie ste založili nový adresár `c:\\ProgramData\\curo-2`, kde sa budú nachádzať všetky dáta a konfigurácie projektu.
Vytvore tu podadresár `c:\\ProgramData\\curo-2\\bin` a skopírujte sem všetky súbory `C:\\Program Files (x86)\\CuroMD\\CuroMdService.*`.

Nastavte configuráciu v `c:\\ProgramData\\curo-2\\bin\\CuroMdService.xml` nasledovne:

.. code-block:: xml

   <configuration>
     <id>CuroMdService2</id>
     <name>CURO MD Server 2</name>
     <!-- Service description -->
     <description>server pre IS PZS CURO 2</description>
     <executable>c:\Program Files (x86)\CuroMD\CuroMD.exe</executable>
     <arguments>-m -d c:\programdata\curo-2</arguments>
   </configuration>


Teraz je možné spravovať túto inštanciu servicu pomocou :bash:`c:\\ProgramData\\curo-2\\bin\\CuroMdService.exe install"`

Reštart Windows Service a spustenie zálohy
++++++++++++++++++++++++++++++++++++++++++

Cez windows Task manager, naimpotujte task do Windows schedulera https://curo.sk/w/servicerestart

Aktualizácia CuroMD bežiaceho ako Windows service
+++++++++++++++++++++++++++++++++++++++++++++++++

#. zastaviť service
#. spustiť inštaláciu, inštalačku nájdete na `%programdata%\\curo\\upgrade\\stable`
#. spustite service

Štruktúra adresára dát ambulancie
---------------------------------

.. todo::  vysvetlit strukturu

Presun CuroMD (server) na nové PC
---------------------------------

Nasledujúce kroky obsahujú celý postup správneho presunu databázy medzi starým a novým PC.

V skratke je potrebné vykonať:

#. na starom PC odinštalovať :term:`CuroMD` a zrušiť zdielanie súborov pacientov
#. na novom PC nainštalovať :term:`CuroMD`, prípadne aj :term:`CuroUI`
#. presunúť dátové súbory zo starého PC na nové
#. nazdielať adresár obsahujúci súbory pacientov z nového PC na každé PC (vrátane nového) ako `setx CURO_DOCUMENTS_PATH <UNC cesta k CURO_PATH_ROOT\storage`:
#. spustiť :term:`CuroMD` na novom PC
#. vyskúšať pripojenie :term:`CuroUI` zo všetkých PC, kde pracujú užívatelia

Inštalácia CURO na novom PC
+++++++++++++++++++++++++++

#. stiahnuť inštalačný súbor :term:`CuroMD`
#. spustiť inštalačný program z disku
#. potvrdiť dialógové okno
#. inštalačný program sa po úspešnom ukončení inštalácie vypne
#. pokiaľ bude PC užívateľ fyzicky pracovať je potrebné zopakovať body 1-4 s :term:`CuroUI`
#. programy zatiaľ nespúšťať

Lokalizácia dátových súborov na starom PC
+++++++++++++++++++++++++++++++++++++++++

Všetky dôležité súbory inštancie :term:`CuroMD` sa nachádzajú zpravidla
v adresári `C:\\ProgramData\\curo`.
V prípade, že sa používa ako databáza CouchDB pozrite sekciu CouchDB nižšie.
Adresár `C:\\ProgramData` je vo Windows nastavený ako neviditeľný pre užívateľa,
preto v súborovom správcovi musíte povoliť zobrazovanie skrytých zložiek, alebo do neho ručne vpísať cestu funguje aj `%programdata%`.

Správcu súborov je možné otvoriť v používanom dátovom adresári aj nasledovne:

#. zapnúť :term:`CuroMD`
#. lokalizovať tray ikonku :term:`CuroMD`
#. aktivovať pravým tlačítkom myši menu nad tray ikonkou
#. zvoliť položku “Lokalita dátových súborov”
#. otvorí sa správca súborov v adresári používanom bežiacou inštanciou :term:`CuroMD`

CouchDB
+++++++
Pre presun CouchDB je potrebné skopirovat adresár `data` z lokácie, kde bolo CouchDB nainštalované, zvykne byť `C:\Program Files\Apache CouchDB\data`
V premenných systému zistite hodnoty pre `CURO_DB_PREFIX` a `CURO_DB_REMOTE` skopírujte si hodnoty a následne vytvorte na novom PC


Odinštalovanie CuroMD zo strého PC
++++++++++++++++++++++++++++++++++

#. vypnúť všetky :term:`CuroUI` na všetkých PC
#. vypnúť CuroMD - cez tray ikonku v pravom dolnom rohu
#. odinštalovať CuroMD
#. zrušiť zdieľanie adresára namapovaného na tom istom PC ako disk `U:` (zvyčajne je to adresár `C:\\ProgramData\\curo\\storage`)

   a. je vhodné si poznačiť ako bolo toto zdieľanie nastavené, prípadne tento krok vykonať až po nastavení zdieľania na novom PC
   b. aby sa zabránilo nesprávnemu ukladaniu súborov na staré PC je veľmi dôležité tento krok vykonať

Presun dátového adresára
++++++++++++++++++++++++

#. adresár ZDROJ, ktorý je potrebné preniesť zo starého PC sme lokalizovali vyššie
#. adresár CIEĽ, na novom PC bude C:\\ProgramData\\curo
#. na novom PC vytvorte adresár CIEĽ
#. celý adresár ZDROJ preneste do adresára CIEĽ (cez USB, ale nápomocné môže byť aj nastavenie zdieľania súborov popísané v nasledujúcej časti)
#. po úspešnom presune je možné adresár ZDROJ zmazať, ale odporúčame to odložiť až o niekoľko dní po vykonaní presunu :term:`CuroMD`

Nastavenie zdieľania pacientových súborov
+++++++++++++++++++++++++++++++++++++++++

#. na novom PC nastavíme zdieľanie adresára C:\\ProgramData\\curo\\storage pre všetkých užívateľov prihlasujúcich sa do Windows a pracujúcich s :term:`CuroUI` na okolitých PC
#. pripojiť toto zdieľanie na všetky používané PC a nastavit ENV premennú CURO_DOCUMENTS_PATH na sietovu cestu `setx CURO_DOCUMENTS_PATH \\SIETOVEMENOPC\storage`

   a. overiť, že sú súbory pre užívateľov viditeľné cez správcu súborov

Test funkčnosti
+++++++++++++++

#. na novom PC spustiť :term:`CuroUI` (spustí automaticky aj :term:`CuroMD`), alebo spustiť priamo :term:`CuroMD`
#. na všetkých PC, kde pracujú užívatelia sa prihlásiť ku :term:`CuroMD`

