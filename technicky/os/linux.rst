Linux
=====

Distribúcie založené na operačnom systéme Linux sú rozmanité. Líšia sa vzhľadom, aktuálnosťou programových balíčkov a spôsobom inštalovania aplikácii.
Komponenty :term:`CuroUI` a :term:`CuroMD` sú plne kompatibilné s každou modernou **64-bitovou** Linux distribúciou, ktorá má **grafické prostredie** a **kernel >=5.7.9**.

Curo komponenty pre Linux sú distribuované vo forme AppImage_, čo zaručuje ich kompatibilitu s ľubovolnou distribúciou. V súčasnosti aj :term:`CuroMD` vyžaduje grafické prostredie, 
v prípade vážneho záujmu nasadiť náš systém na tzv. **headless** serveri sme schopný také riešenie dodať.

Naše skúsenosti s nasadením Curo na PC s operačným systémom sú veľmi dobré. Odozva systému je rýchlejšia na rovnakom stroji s Windows a aktualizácie nespôsobujú neočakávané
spomalenia systému a dlhé čakanie pri spustení PC. Nároky a finančné náklady na hardware sú nižšie. V porovnaní s prostredím Windows evidujeme minimum komplikácií spôsobených aktualizáciami a chybami v operačnom systéme.

Žiaľ, z dôvodu **nepripravenosti projektu eZdravie** na iné operačné systémy ako je Windows je potrebné v ambulanci použiť PC s Windows 
na pripojenie čítačiek ePZP_ kariet.

Pri správnej voľbe distribúcie a začiatočnej konfigurácii je správa Linuxovej stranice porovnateľne jednoduchá ako PC s Windows. V prípade, že nemáte ešte Linux stanicu ani vybratú Linux distribúciu,
odporúčame distribúciu Solus_ s desktopom Budgie_.

|Budgie|

Čo mať na pamäti
****************

Pri rozhodovaní sa o použití Linux pracovnej stanice je potrebné mať na paäti:

* správna voľba distribúcie a počiatočná konfigurácia umožnia bezproblémovú prevádzku dlhé roky
* eZdravie vyžaduje PC s Windows na pripojenie čítačky ePZP_ karty a inštaláciu :term:`Curo eZdravie`
* web, email, textový, tabuľkový editor, ani tvorba prezentácií nie je problém, no niektoré zdravotnícke prístroje majú aplikácie kompatibilné len s Windows

Podpora
*******

Sme presvädčený, že operačný systém Linux je vhodná voľba pre väčšinu pracovných staníc ambulancie a sme príjemne prekvapený, že stretávame stále viac užívateľov, ktorý nemajú obavy
vyskúšať a následne úspešne používať Curo na Linuxe.

Budeme radi ak sa s nami podelíte o svoje skúsenosti s Linux a v prípade potreby poradíme. Kontaktujte nás na podpora_.

.. _AppImage: https://appimage.org/
.. _Solus: https://distrowatch.com/table.php?distribution=solus
.. _Budgie: https://en.wikipedia.org/wiki/Budgie_(desktop_environment)
.. _MATE: https://en.wikipedia.org/wiki/MATE_(software)
.. _ePZP: https://www.ezdravotnictvo.sk/sk/-/zp-instalacia-citacky-a-karty-epzp
.. _podpora: mailto:podpora@curo.sk.

.. |Budgie| image:: https://upload.wikimedia.org/wikipedia/commons/8/8d/Budgie_%28desktop_environment%29_v10.4.png
  :width: 400
  :alt: Ukážka Budgie desktopu
