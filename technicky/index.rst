Technický zástupca
==================

.. toctree::
   :maxdepth: 3
   :caption: Obsah:

   components/index
   os/linux
   reports
   zalohovanie
   
Sieťová konektivita
-------------------

porty
ako UI nájde MD

Zdielanie dokumentov
--------------------

Zmeny nastaveni 
---------------

cez Curo support

Vzory
-----

Cenníky
-------

Vlastné výkony
++++++++++++++

Export dát
----------

Import dát
----------

Oprava konzistencie indexov
---------------------------

      process.env.CURO_FLAG_BOOT_QUERY_VIEWS === 'recreate' || // drop and rebuild all queries defined in datasource.json
      process.env.CURO_FLAG_BOOT_QUERY_VIEWS === 'rebuild' || // drop and rebuild all queries based on DB definitions
      process.env.CURO_FLAG_BOOT_QUERY_VIEWS === 'missing' || // rebuild all missing queries defined in datasource.json

set CURO_TOOLS="reinit,info,compact,fauxton"

* reinit - špeciálna databáza `pouch__all_dbs__` obsahuje zoznam databáz a tento príkaz ju naplní ak neexistuje alebo je nekompletná
* info - zobrazí zoznam databáz, ich počet dokumentov
* compact - prekontroluje a potencionálne zmenší databázy
* fauxton - na porte 5984 sa spustí
