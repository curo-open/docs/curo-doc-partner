.. _zalohovanie:

Zálohovanie
===========

Automatické spustenie zálohy
----------------------------

Záloha v sytéme Curo sa vykoná automaticky pri každom štarte Curo MD ak ubehlo viac ako 20 hodín od poslednej zálohy.

Východzie úložisko pre zálohu je `%appdata%\\curo-zaloha`, toto správanie sa dá zmeniť v súbore `%programdata%\\curo\\curo-md.config.js`

.. code-block:: javascript
  :linenos:
  :emphasize-lines: 6

    'backup': {
      'disabled': false,
      'backup': true,
      'buildIn': true,
      'mode': 'quick', // 'quick' fast no export run
      'target': 'c:\\zaloha-test'
    }

na riadku 6 viete zmeniť cestu kam sa záloha uloží, odporúčame externý disk.


Postup ak bežite Curo MD ako Windows Service
++++++++++++++++++++++++++++++++++++++++++++

Ak bežite Curo MD ako service lekár počitač nevypína, vtedy automatickú zálohu docielime reštartom servisu.

#. naimpotujte `tento task <https://curo.sk/w/servicerestart>`_ do windows Task Schedulera
#. upravte usera
#. uložte

Manuálne spustenie zálohy
-------------------------

Pozrite `tento návod <https://curois.freshdesk.com/support/solutions/articles/33000256267>`_