.. _glossary:

Slovník pojmov
==============

.. glossary::

   CuroMD
      server; jedna inštancia pre ambulanciu; spravidla na najsilnejšom PC; spúšťa sa samostatne alebo si ho spustí CURO UI ak sú na rovnakom PC; 
      zabezpečuje ukladanie dát do lokálnej databázy a ich odkomunikovanie s externými službami (NCZI eZdravie, zdr. poisťovne, laboratóriá)

   CuroUI
      program užívateľského rozhrania; musí byť nainštalovaný na každom PC, kde pracuje užívateľ CURO; sieťovo komunikuje s :term:`CuroMD`
   
   Curo eZdravie
      komunikuje s HPRO kartou lekára a službami eZdravia; inštalovaný by mal byť na PC, kde pracuje lekár, aby mohol zadávať PIN; 
      CURO MD musí byť nakonfigurovaný tak, aby vedel sieťovo s CURO eZdravie komunikovať

   dátový adresár
      adresár obsahujúci všetko (dáta, číselníky, konfigurácie, logy, fronty spracovania) okrem samotnej aplikácie CURO MD; zvyčajne je to `C:\\ProgramData\\curo`

   konfiguračný adresár CuroUI      
      | Windows: `%APPDATA%\\curo`, čo je zvyčajne `C:\\Users\\UŽÍVATEĽ\\AppData\\Roaming`
      | macOS: `~/Library/Application Support/Curo`
      | Linux: `$XDG_CONFIG_HOME\Curo` alebo `~/.config/Curo`


   adresár súborov pacientov
      adresár obsahujúci externe získané súbory (fotky, scany) pacienta, ktoré CURO dokáže pre konkrétneho pacienta sprístupniť; 
      samotný adresár nie je pre CURO dôležitý, podstatné je až jeho zdieľanie cez disk U:; 
      zvyčajne je umiestnený pod dátovým adresárom a jeho cesta je `C:\\ProgramData\\curo\\public\\dokumenty`

   disk U:
      CURO UI predpokladá, že súbory pacientov sa nachádzajú na disku U:; tento disk je potrebné sieťovo namapovať na každom PC, kde sa nachádza CURO UI; 
      disk U: má byť zdieľaný obsah adresára súbory pacientov
   
   správca súborov
      File Explorer, Total Commander, muCommander

   Windows Service
      Služba Windows je v operačnom systéme Microsoft Windows špeciálny program, ktorý je spustený dlhodobo a nie je v priamom kontakte s užívateľom
      (na rozdiel od běžných aplikácií). Pre viac detailov https://en.wikipedia.org/wiki/Windows_service.
   