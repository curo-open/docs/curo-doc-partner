.. _obsah:

Curo dokumentácia pre partnerov
===============================

obchodných a technických reprezentantov

.. toctree::
   :maxdepth: 2
   :caption: Obsah:

   uvod/index

   obchodny/index
   technicky/index
   integracie/index
   glossary

.. todolist::   

Zoznamy a tabuľky
=================

.. only:: builder_html

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
   * :ref:`glossary`

.. only:: not builder_html

   * :ref:`modindex`
   * :ref:`glossary`
